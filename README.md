# RPS (ru)

Простая игра Камень, ножницы, бумага написанная на Python.

Для запуска используйте файл `rps.py`

# RPS (en)

Simple rock, paper, scissors game written on Python.

To run use `rps_en.py`

# Внесение вклада (ru)

Руки чешутся что-то добавить или исправить? Сделай запрос на слияние!

# Contributing (en)

You want to add or correct something? Just make pull request!